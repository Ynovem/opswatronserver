# OpswatronServer

LD_LIBRARY_PATH

# TODO

1) user management
	1) AD integration for colleagues
	1) Registration/Add user possibility for akademia's participants.
1) db side
	1) historical data (database)
	1) show top N
1) game
	1) replay battles
	1) handle different game types: one-vs-one, battle royale

### define necessary routes

##### Viewes

* index.html

	login possibility

* home.html

	home

* user.html

	user settings

##### REST

* /login

	restriction: -
	description: login

* /logout

	restriction: authenticated user
	description: logout

* /reg

	restriction: admin
	description: add new user

* /user

	restriction: admin
	description: list of users

* /user

	restriction: admin
	description: list of users

* db
    db.find()
    ->true
        ldap?
        ->true
            ldap.authenticate()
            ->true
                sync db + 200
            ->false
                401 BAD PASSWORD
        ->false
            checkPassword()
    ->false
        ldap.find()
        ->true
        ->false

### BEFORE PRODUCTION

* use `ng build` and static file-serving without use `ng serve` with proxy
* (best practice performance)[https://expressjs.com/en/advanced/best-practice-performance.html]

### USEFUL minden

* (angular with express)[https://levelup.gitconnected.com/simple-application-with-angular-6-node-js-express-2873304fff0f]

