var uuid = require('uuid/v4');
var bcrypt = require('bcryptjs');
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database(':memory:');
// var db = new sqlite3.Database('../sqlite.db');

var userSchema = `
CREATE TABLE IF NOT EXISTS user (
	id INTEGER NOT NULL PRIMARY KEY,
	username TEXT UNIQUE,
	password TEXT,
	ldap BOOLEAN
);
`;


var playersSchema = `
CREATE TABLE IF NOT EXISTS players (
	id INTEGER NOT NULL PRIMARY KEY,
	name TEXT,
	filename TEXT
);
`;

var matchesSchema = `
CREATE TABLE IF NOT EXISTS matches (
	id INTEGER NOT NULL PRIMARY KEY,
	x INTEGER NOT NULL,
	y INTEGER NOT NULL,
	filename STRING NOT NULL,
	finished INTEGER DEFAULT 0
);
`;
var matchesPlayersSchema = `
CREATE TABLE IF NOT EXISTS matches_players (
	id INTEGER NOT NULL PRIMARY KEY,
	player_id INTEGER NOT NULL,
	match_id INTEGER NOT NULL,
	FOREIGN KEY (player_id) REFERENCES players(id),
	FOREIGN KEY (match_id) REFERENCES matches(id)
);
`;

db.serialize(() => {
	db.get("PRAGMA foreign_keys = ON");
	db.run(userSchema);
	// db.run(playersSchema);
	// db.run(matchesSchema);
	// db.run(matchesPlayersSchema);
});

let salt = bcrypt.genSaltSync(10);

let crypt = function(pwd) {
	return bcrypt.hashSync(pwd, salt);
};

const services = {
	users: {
		list: (cb) => {
			db.all('SELECT * FROM user', cb);
		},
		get: (id, cb) => {
			db.run('SELECT * FROM user WHERE id = $id LIMIT 1', {$id: id}, cb);
		},
		findByUsername: (username, cb) => {
			db.run('SELECT * FROM user WHERE username = $username LIMIT 1', {$username: username}, cb);
		},
		checkPassword: (password, user) => {
			return bcrypt.compareSync(password, user['password']);
		},
		update: (id, cb) => {
			cb();
			// NOTE pass
		},
		create: (username, password, ldap, cb) => {
			db.run('INSERT INTO user (username, password, ldap) VALUES ($username, $password, $ldap)', {$username: username, $password: crypt(password), $ldap: ldap}, cb);
		},
		remove: (id, cb) => {
			cb();
			// NOTE pass
		}
	},
	players: {
		list: (cb) => {
			db.all('SELECT * FROM players', cb);
		},
		create: (user, filename, cb) => {
			db.run('INSERT INTO players (name, filename) VALUES ($name, $filename)', {$name: user, $filename: filename}, cb);
		},
		remove: (id, cb) => {
			cb();
		}
	},
	matches: {
		list: (cb) => {
			db.all('SELECT * FROM matches', cb);
		},
		get: (id, cb) => {
			db.get('SELECT * FROM matches WHERE matches.id = $id', {$id: id}, function(err, match){
				if (err) {
					cb(err, null);
				} else {
					db.all('SELECT * FROM players WHERE id IN (SELECT player_id FROM matches_players WHERE match_id = $id)', {$id: id}, function(err, players){
						if (err) {
							cb(err, null);
						} else {
							match['players'] = players;
							cb(err, match);
						}
					});
				}
			});
		},
		create: (x, y, cb) => {
			db.run('INSERT INTO matches (x, y, filename) VALUES ($x, $y, $filename)', {$x: x, $y: y, $filename: uuid()}, function(err){
				cb(err, this.lastID);
			});
		},
		finished: (id, cb) => {
			db.run('UPDATE matches SET finished = 1 WHERE id = $id', {$id: id}, cb);
		},
		addPlayer: (playerId, matchId, cb) => {
			db.run('INSERT INTO matches_players (player_id, match_id) VALUES ($playerId, $matchId)', {
				$playerId: playerId,
				$matchId: matchId
			}, cb);
		},
		addPlayers: (playerIds, matchId, cb) => {
			var stmt = db.prepare("INSERT INTO matches_players (player_id, match_id) VALUES ($playerId, $matchId)");
			for (var i = 0; i < playerIds.length; i++) {
				stmt.run({
					$playerId: playerIds[i],
					$matchId: matchId
				});
			}
			stmt.finalize();
		}
	}
};

db.serialize(() => {
	services.users.create("User1", "password1", false);
	services.users.create("User2", "password2", false);
});

/*
db.serialize(() => {
	services.players.create("A", "aa");
	services.players.create("B", "bb");
	services.matches.create(10, 10, (err, id) => {
		db.serialize(() => {
			services.matches.addPlayers([1, 2], 1);
			services.matches.finished(1);
			services.matches.get(1, console.log);
		});
	});

	services.matches.list(console.log);
	services.matches.get(1, console.log);
});
*/

module.exports = services;
/*
// test1
setTimeout(() => {
	db.matches.create(10, 10, (err, id) => {
		console.log(`Match created ${id}`);
		db.matches.list((err, data) => {
			console.log(`Match list`, data);
			db.matches.finished(id, () => {
				console.log(`Match finished`);
				db.matches.list((err, data) => {
					console.log(`Match list`, data);
				})
			})
		})
	});
}, 1000);
*/
