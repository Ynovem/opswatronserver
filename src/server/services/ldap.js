let ActiveDirectory = require('activedirectory');

let config = {
	url: process.env.LDAP_URL,
	baseDN: process.env.LDAP_BASEDN,
	username: process.env.LDAP_USERNAME,
	password: process.env.LDAP_PASSWORD,
};

const domain = process.env.LDAP_DOMAIN;

let ldap = new ActiveDirectory(config);

const authenticate = (username, password) => {
	return new Promise((resolve, reject) => {
		ldap.authenticate(`${domain}\\${username}`, password, function(error, auth) {
			if (error) {
				// TODO logging
				console.error(`ERROR: ldap.authenticate[${username}]`, error.name, error.message);
				reject();
			} else {
				resolve(auth);
			}
		});
	}).catch(()=> false);
};

const findUser = (username) => {
	return new Promise((resolve, reject) => {
		ldap.findUser(username, function(error, user) {
			if (error) {
				// TODO logging
				console.error(`ERROR: ldap.findUser[${username}]`, error.name, error.message);
				reject();
			} else {
				resolve(user);
			}
		});
	}).then(user => user || null).catch(() => null);
};

module.exports = {
	authenticate: authenticate,
	findUser: findUser
};
