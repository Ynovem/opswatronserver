var express = require('express');
var path = require('path');
var router = express.Router();
// var db = require('../services/db');
var { Player } = require('../models/player.model');

const {spawn} = require('child_process');

const prefix = "/home/opswatron";
const opswatron = `${prefix}/bins/server/opswatron`;
const playersPrefix = `${prefix}/players`;
const rootPath = path.join(__dirname, '..', '..', '..');
const sourceMount = `${rootPath}:${prefix}`;
// var opswatron = "/home/gfeher/workspace/opswat/innovation-day/opswatron-server/test/server/opswatron";
var configs = {
	"-t": 1000,
	"-x": 30,
	"-y": 30
};

console.log(sourceMount);
router.get('/', function (req, res, next) {
	new Promise((resolve, reject) => {
		// db.matches.create(configs.x, configs.y, (err, id) => {
		//
		// });
		Player.list().then((players) => {
			const cmdArray = [];
			players.forEach((player) => {
				cmdArray.push(`chmod +x ${playersPrefix}/${player.filename} &&`);
			});
			console.log(players);
			console.log(cmdArray);

			cmdArray.push(`LD_LIBRARY_PATH=${prefix}/libs`);
			cmdArray.push(opswatron);

			players.forEach((player) => {
				cmdArray.push("-p");
				cmdArray.push(`${playersPrefix}/${player.filename}`);
			});

			Object.keys(configs).forEach((key) => {
				cmdArray.push(key);
				cmdArray.push(configs[key]);
			});
			// -p ${prefix}/bins/players/avoid -p ${prefix}/bins/players/avoid -t 100 -x 10 -y 10
			const ls = spawn("docker", [
				"run",
				"--rm",
				"-i",
				"-v", sourceMount,
				"ubuntu",
				"bash",
				"-c",
				cmdArray.join(' ')
			]);
			var steps = [];
			ls.stdout.on('data', (data) => {
				data.toString('utf8').split('\n').forEach((row) => {
					row = row.trim();
					if (row !== "") {
						steps.push(JSON.parse(row));
					}
				});
			});

			ls.stderr.on('data', (data) => {
				console.log(`stderr: ${data}`);
			});

			ls.on('close', (code) => {
				resolve(steps);
				console.log(`close: ${code}`)
			});
			ls.on('error', console.log);
		});
	})
	.then(response => {
		res.json(response);
	})
	.catch(reject => {
		res.send(reject);
	});
});

module.exports = router;
