const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');
const upload = require('../middlewares/uploader');
const { Player } = require("../models/player.model");
const { User } = require("../models/user.model");

router.get('/', auth, async function(req, res, next) {
	const players = await Player.list();
	return res.json(players);
});

// router.post('/', upload.single('player'), (req, res, next) => {
// 	db.players.create(req.body.user, req.file.filename, () => {
// 		res.send(`${req.body.user} saved`);
// 	});
// });
// router.use('/', auth);
router.post('/', auth, upload.single('player'), async function(req, res, next) {
	console.log("try to save: ");
	console.log("\t", req.user);
	console.log("\t", req.body, Object.keys(req.body));
	console.log("\t", req.file.filename);
	// get logged in users id!!!
	const player = new Player({
		id: null,
		name: req.body.playername,
		filename: req.file.filename,
		userId: req.user.id,
	});
	console.log(await User.list());
	await player.save();
	res.status(201).json({});
});

module.exports = router;
