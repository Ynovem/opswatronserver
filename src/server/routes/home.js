var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
	res.render('index', { title: 'Express' });
});

router.get('/display1', function(req, res, next) {
	res.render('play1', { title: 'Express' });
});

router.get('/display2', function(req, res, next) {
	res.render('play2', { title: 'Express' });
});

router.get('/display3', function(req, res, next) {
	res.render('play3', { title: 'Express' });
});

module.exports = router;
