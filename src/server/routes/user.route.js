let express = require('express');
let router = express.Router();
const auth = require('../middlewares/auth');
let ldap = require('../services/ldap');
let { User } = require("../models/user.model");

router.post('/login', async function (req, res, next) {
	const username = req.body.username;
	const password = req.body.password;
	let user = await User.findByUsername(username);

	// TODO 403 -> 401
	if (user && !user.ldap) {
		console.log("LOGIN DB USER", user);
		if (user.checkPassword(password)) {
			const token = user.generateToken();
			return res.status(201).json({token: token})
		} else {
			return res.status(403).json({error: "auth error"});
		}
	}

	if (!user || user.ldap) {
		if(!await ldap.authenticate(username, password)) {
			return res.status(403).json({error: "auth error"});
		}

		const ldapUser = await ldap.findUser(username);

		if (ldapUser == null) {
			return res.status(403).json({error: "auth error"});
		}

		if (!user) {
			user = new User({});
		}
		user.username = ldapUser['sAMAccountName'];
		user.password = null;
		user.ldap = true;
		await user.save();

		const token = user.generateToken();

		return res.status(201).json({token: token})
	}

	return res.status(403).json({error: "auth error"});

});

router.post('/user', auth, function(req, res, next) {
	// const username = req.body.username;
	// const password = req.body.password;
	//
	// db.users.create(username, password, false);
	//
	// db.users.list((err, users) => {
	// 	res.json({err, users})
	// });
});

router.get('/user', auth, async function(req, res, next) {
	const users = await User.list();
	return res.json(users);
});

module.exports = router;
