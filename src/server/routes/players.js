var express = require('express');
var router = express.Router();
var db = require('../services/db');
var upload = require('../middlewares/uploader');

router.get("/", (req, res, next) => {
	db.players.list((err, rows) => {
		res.json(rows);
	});
});

router.post('/', upload.single('player'), (req, res, next) => {
	db.players.create(req.body.user, req.file.filename, () => {
		res.send(`${req.body.user} saved`);
	});
});

router.delete('/:id', upload.single('player'), (req, res, next) => {
	console.log(`${req.path.id} removed`);
	res.send(`${req.path.id} removed`);
});

module.exports = router;
