let createError = require('http-errors');
let app = require('../app');

app.use('/', require('./user.route'));
app.use('/player', require('./player.route'));

if (!process.env.NODE_ENV || process.env.NODE_ENV !== 'production') {
	(async () => {
		let log = (level) => {
			// 1 INFO
			// 2 DEBUG
			// 3 DUMP
			let verboseLevel = 0;
			return level <= verboseLevel;
		};

		let { Player } = require("../models/player.model");
		let { User } = require("../models/user.model");

		const user1 = new User({username: "User1", password: "password1", ldab: false});
		log(2) && console.log("New User: ", user1);
		await user1.save();
		log(2) && console.log("\tsaved");

		const user2 = new User({username: "User2", password: "password2", ldab: false});
		log(2) && console.log("New User: ", user2);
		await user2.save();
		log(2) && console.log("\tsaved");

		const users = await User.list();
		log(1) && console.log("Users: ", users);

		const player1 = new Player({name: 'Player1', filename: 'norbitron_av', userId: 1});
		log(2) && console.log("New Player: ", player1);
		await player1.save();
		log(2) && console.log("\tsaved");

		const player2 = new Player({name: 'Player2', filename: 'norbitron_av-ag', userId: 1});
		log(2) && console.log("New Player: ", player2);
		await player2.save();
		log(2) && console.log("\tsaved");

		const player3 = new Player({name: 'Player3', filename: 'norbitron_av-dw', userId: 2});
		log(2) && console.log("New Player: ", player3);
		await player3.save();
		log(2) && console.log("\tsaved");

		const players = await Player.list();
		log(1) && console.log("Players: ", players);
	})();
}


// app.use('/', require('./home'));
app.use('/play', require('./play'));
// app.use('/player', require('./players'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	next(createError(404));
});
