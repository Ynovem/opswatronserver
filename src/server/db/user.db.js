const bcrypt = require('bcryptjs');
const db = require('./schema');

const salt = bcrypt.genSaltSync(10);
const crypt = function(pwd) {
	return bcrypt.hashSync(pwd, salt);
};

const list = () => {
	const query = 'SELECT * FROM user';

	return new Promise((resolve, reject) => {
		db.all(query, (error, users) => {
			if (error) {
				reject(error);
			} else {
				resolve(users);
			}
		});
	});
};

const findById = (id) => {
	const query = 'SELECT * FROM user WHERE id = $id';

	return new Promise((resolve, reject) => {
		db.get(query, {$id: id}, function(error, user){
			if (error) {
				reject(error);
			} else {
				resolve(user);
			}
		});
	});
};

const findByUsername = (username) => {
	const query = 'SELECT * FROM user WHERE username = $username LIMIT 1';

	return new Promise((resolve, reject) => {
		db.get(query, {$username: username}, function (error, user){
			if (error) {
				reject(error);
			} else {
				resolve(user);
			}
		});
	});
};

const create = (username, password, ldap) => {
	const query = 'INSERT INTO user (username, password, ldap) VALUES ($username, $password, $ldap)';

	return new Promise((resolve, reject) => {
		if (password) {
			password = crypt(password);
		}
		db.run(query, {$username: username, $password: password, $ldap: ldap}, function (error){
			if (error) {
				reject(error);
			} else {
				resolve(this.lastID);
			}
		});
	});
};

const update = (id, username, ldap) => {
	const query = 'UPDATE user SET username = $username, ldap = $ldap WHERE id = $id';

	return new Promise((resolve, reject) => {
		db.run(query, {$id: id, $username: username, $ldap: ldap}, (error, user) => {
			if (error) {
				reject(error);
			} else {
				resolve(user);
			}
		});
	});
};

const remove = id => {
	const query = 'DELETE FROM user WHERE id = $id';

	return new Promise((resolve, reject) => {
		db.run(query, {$id: id}, function(error){
			if (error) {
				reject(error);
			} else {
				resolve(this.changes);
			}
		});
	});
};

module.exports = {
	list: list,
	findById: findById,
	findByUsername: findByUsername,
	create: create,
	update: update,
	remove: remove
};
