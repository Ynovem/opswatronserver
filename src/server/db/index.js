let userDb = require('./user.db');
let playerDb = require('./player.db');

module.exports = {
	users: userDb,
	players: playerDb
};
