const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(':memory:');
// const db = new sqlite3.Database('../sqlite.db');

const userSchema = `
CREATE TABLE IF NOT EXISTS user (
	id INTEGER NOT NULL PRIMARY KEY,
	username TEXT UNIQUE,
	password TEXT,
	ldap BOOLEAN
);
`;

const playerSchema = `
CREATE TABLE IF NOT EXISTS player (
	id INTEGER NOT NULL PRIMARY KEY,
	name TEXT UNIQUE,
	filename TEXT,
	user_id INTEGER NOT NULL,
	FOREIGN KEY (user_id) REFERENCES user(id)
);
`;

db.serialize(() => {
	db.get('PRAGMA foreign_keys = ON');
	db.run(userSchema);
	db.run(playerSchema);
});

module.exports = db;
