const userDb = require('../db/user.db');

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

class User {
	constructor(data) {
		this.id = data.id || null;
		this.username = data.username;
		this.password = data.password;
		this.ldap = data.ldap || false;
	}

	generateToken() {
		return this.token = jwt.sign({id: this.id, username: this.username, ldap: this.ldap}, process.env.JWT_KEY, {expiresIn:  '1h'});
	}

	save() {
		if (this.id) {
			return userDb.update(this.id, this.username, this.ldap);
		} else {
			// Note: set id of new user
			return userDb.create(this.username, this.password, this.ldap).then(id => {this.id = id});
		}
	}

	remove() {
		return userDb.remove(this.id);
	}

	checkPassword(password) {
		return bcrypt.compareSync(password, this.password);
	}

	static list() {
		return userDb.list().then(users => users.map(user => new User(user)));
	};

	static findById(id) {
		return userDb.findById(id).then(user => new User(user));
	};

	static findByUsername(username) {
		return userDb.findByUsername(username).then(user => {
			if (user) {
				return new User(user);
			} else {
				return null;
			}
		});
	};
}

module.exports = {
	User: User
};
