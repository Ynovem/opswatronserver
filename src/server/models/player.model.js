// const uuid = require('uuid/v4');
const playerDb = require('../db/player.db');

class Player {
	constructor(data) {
		this.id = data.id || null;
		this.name = data.name || "";
		this.filename = data.filename || "";
		// db attr sync user_id vs userId
		this.userId = data.userId || data['user_id'] || null;
	}

	save() {
		// if (this.id) {
		// 	return userDb.update(this.id, this.username, this.ldap);
		// } else {
			return playerDb.create(this.name, this.filename, this.userId);
		// }
	}

	static list() {
		return playerDb.list().then(players => players.map(player => new Player(player)));
	};
}

module.exports = {
	Player: Player
};
