var uuid = require('uuid/v4');
const multer = require('multer');
const path = require('path');
const destination = path.join(__dirname, '..', '..', '..', 'players');

// SET STORAGE
const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, destination)
	},
	filename: function (req, file, cb) {
		cb(null, uuid())
	}
});

const upload = multer({ storage: storage });

module.exports = upload;
