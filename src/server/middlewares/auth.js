const jwt = require('jsonwebtoken');

// function findUser() {
// 	// ad.findUser(req.body.username, (err, user) => {
// 	// 	return user;
// 	// });
// }

const auth = async(req, res, next) => {
	console.log("auth middleware");
	const token = req.cookies['opswatron-token'];
	console.log("auth: ", token);

	try {
		// const data = jwt.verify(token, process.env.JWT_KEY);
		// req.user = findUser(data);
		req.user = jwt.verify(token, process.env.JWT_KEY);
		next();
	} catch(error) {
		res.status(401).send({error: 'Not authorized to access this resource'});
	}
};

module.exports = auth;

