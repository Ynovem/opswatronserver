import { Component, OnInit } from '@angular/core';

declare var $: any;
declare var jQuery: any;

@Component({
	selector: 'app-opswatron',
	templateUrl: './opswatron.component.html',
	styleUrls: ['./opswatron.component.scss']
})
export class OpswatronComponent implements OnInit {

	constructor() { }

	ngOnInit() {
		$.get('/api/play', (data) => {
			const x = 19;
			const y = 19;
			const parts = [];
			const GAME_SPEED = 100;
			const CANVAS_BORDER_COLOUR = 'black';
			const CANVAS_BACKGROUND_COLOUR = 'white';

			const gameCanvas: any = document.getElementById('gameCanvas');
			const ctx = gameCanvas.getContext('2d');

			const snakeColours = [
				{fill: 'lightgreen', border: 'darkgreen'},
				{fill: 'lightblue', border: 'darkblue'},
				{fill: 'red', border: 'red'},
			];

			data[0].players.forEach((player) => {
				parts.push({x: player.coords[0], y: player.coords[1],
					id: player.id});
			});
			data[0].walls.forEach((wall) => {
				parts.push({x: wall.coords[0], y: wall.coords[1],
					id: wall.player_id
				});
			});
			let iteration = 0;
			main();

			function getColour(i) {
				if (i < snakeColours.length) {
					return snakeColours[i];
				} else {
					return snakeColours[0];
				}
			}
			function main() {
				setTimeout(function onTick() {
					if (iteration < data.length && !data[iteration].end) {

						clearCanvas();
						data[iteration].players.forEach((player) => {
							parts.push({
								x: player.coords[0],
								y: player.coords[1],
								id: player.id
							});
						});
						data[iteration].walls.forEach((wall) => {
							parts.push({
								x: wall.coords[0],
								y: wall.coords[1],
								id: wall.player_id
							});
						});
						drawSnake();

						iteration++;
						main();
					}
				}, GAME_SPEED)
			}

			function clearCanvas() {
				ctx.fillStyle = CANVAS_BACKGROUND_COLOUR;
				ctx.strokestyle = CANVAS_BORDER_COLOUR;

				ctx.fillRect(0, 0, gameCanvas.width, gameCanvas.height);
				ctx.strokeRect(0, 0, gameCanvas.width, gameCanvas.height);
			}

			function drawSnake() {
				parts.forEach(drawSnakePart)
			}

			function drawSnakePart(snakePart) {
				// ctx.fillStyle = snake_colours[0].fill;
				// ctx.strokestyle = snake_colours[0].border;
				console.log(snakePart);
				ctx.fillStyle = getColour(snakePart.id).fill;
				ctx.strokestyle = getColour(snakePart.id).border;
				ctx.fillRect(snakePart.x * 10, snakePart.y * 10, 10, 10);
				ctx.strokeRect(snakePart.x * 10, snakePart.y * 10, 10, 10);
			}
		});
	}

}
