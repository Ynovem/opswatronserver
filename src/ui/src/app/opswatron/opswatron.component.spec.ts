import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpswatronComponent } from './opswatron.component';

describe('OpswatronComponent', () => {
  let component: OpswatronComponent;
  let fixture: ComponentFixture<OpswatronComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpswatronComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpswatronComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
