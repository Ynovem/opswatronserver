import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {AuthService} from '../services/auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup;

	constructor(private fb: FormBuilder, private http: HttpClient, private cookieService: CookieService, private authService: AuthService) {
		this.loginForm = this.fb.group({
			username: [''],
			password: ['']
		});
	}

	ngOnInit() {
	}

	onClickSubmit() {
		this.http.post('/api/login', this.loginForm.value).subscribe(
			(data: any) => {
				this.authService.login(data.token);
			},
			(error) => {
				console.log('error', error);
			}
		);
	}

}
