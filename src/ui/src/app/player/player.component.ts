import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FileUploader } from 'ng2-file-upload';

@Component({
	selector: 'app-player',
	templateUrl: './player.component.html',
	styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
	public uploader: FileUploader = new FileUploader({
		url: '/api/player',
		itemAlias: 'player'
	});
	public playername: string;
	public players: [];
	constructor(private http: HttpClient) { }

	ngOnInit() {
		this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
		this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
			console.log('ImageUpload:uploaded:', item, status, response);
		};
		this.uploader.onBuildItemForm = (item, form) => {
			form.append('playername', this.playername);
		};
	}

	getPlayers() {
		this.http.get('/api/player').subscribe(
			(data: any) => {
				this.players = data;
				console.log(data);
			}
		);
	}

}
