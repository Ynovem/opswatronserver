import { Component, OnInit } from '@angular/core';
import {AuthService} from './services/auth.service';

// https://www.positronx.io/angular-8-express-file-upload-tutorial-with-reactive-forms/
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	constructor(private authService: AuthService) {
	}

	ngOnInit() {
	}

	isAuthenticated() {
		return this.authService.isAuthenticated();
	}

	logout() {
		this.authService.logout();
	}
}
