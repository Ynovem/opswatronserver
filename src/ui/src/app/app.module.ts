import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FileSelectDirective } from 'ng2-file-upload';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { CookieService } from 'ngx-cookie-service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { PlayerComponent } from './player/player.component';
import { OpswatronComponent } from './opswatron/opswatron.component';

/*
const routes: Routes = [
	{
		path: '',
		component: InputUserDataFormComponent
	},
	{
		path: 'user/:uid',
		component: DisplayUserDataComponent
	}
];
*/
@NgModule({
	declarations: [
		AppComponent,
		FileSelectDirective,
		LoginComponent,
		UserComponent,
		PlayerComponent,
		OpswatronComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,

		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,

		NgbModule,
		NgbAlertModule,
	],
	providers: [
		{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
		CookieService
	],
	bootstrap: [AppComponent]
})

export class AppModule { }
