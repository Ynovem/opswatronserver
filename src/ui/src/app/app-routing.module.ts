import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleGuardService } from './guards/role-guard.service';
import { AuthGuardService } from './guards/auth-guard.service';

import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { PlayerComponent } from './player/player.component';
import {OpswatronComponent} from './opswatron/opswatron.component';


const routes: Routes = [
	// {
	// 	path: '',
	// 	component: InputUserDataFormComponent
	// },
	{
		path: 'login',
		component: LoginComponent,
	},
	{
		path: 'user',
		component: UserComponent,
		// canActivate: [AuthGuardService],
	},
	// {
	// 	path: 'user',
	// 	component: UserComponent,
	// 	canActivate: [AuthGuardService, RoleGuardService],
	// 	data: {role: 'Admin'}
	// },
	{
		path: 'player',
		component: PlayerComponent,
	},
	{
		path: 'opswatron',
		component: OpswatronComponent,
	},
	{
		path: '**',
		redirectTo: '/login',
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
