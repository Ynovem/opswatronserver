import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import decode from 'jwt-decode';
import {CookieService} from 'ngx-cookie-service';
import {User} from '../models/user';


@Injectable({
	providedIn: 'root'
})
export class AuthService {
	private readonly tokenName: string;
	private currentUser: User;

	constructor(private router: Router, private cookieService: CookieService) {
		this.tokenName = 'opswatron-token';
	}

	clear(): void {
		this.currentUser = null;
		this.cookieService.delete(this.tokenName);
	}

	isAuthenticated(): boolean {
		return this.cookieService.get(this.tokenName) != null && !this.isTokenExpired();
	}

	isTokenExpired(): boolean {
		return Date.now() / 1000 < this.decode(this.cookieService.get(this.tokenName)).exp;
	}

/*
	loginAdmin(): void {
		localStorage.setItem('token',
		`eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
		eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLC
		JpYXQiOjE1MzMyNzM5NjksImV4cCI6MTU2NDgx
		MDAwNSwiYXVkIjoid3d3LmV4YW1wbGUuY29tIi
		wic3ViIjoiVGVzdCBHdWFyZCIsIkdpdmVuTmFt
		ZSI6IkpvaG5ueSIsIlN1cm5hbWUiOiJSb2NrZX
		QiLCJFbWFpbCI6Impyb2NrZXRAZXhhbXBsZS5j
		b20iLCJyb2xlIjoiQWRtaW4ifQ.rEkg53_IeCL
		zGHlmaHTEO8KF5BNfl6NEJ8w-VEq2PkE`);

		this.router.navigate(['/dashboard']);
	}
*/

	getToken(): string {
		return this.cookieService.get(this.tokenName);
	}

	getCurrentUser(): User | null {
		return this.currentUser;
	}

	login(token: any): void {
		const data = this.decode(token);
		if (!data) {
			this.logout();
		} else {
			this.currentUser = new User(data.id, data.username, data.token);
			this.cookieService.set(this.tokenName, token, data.exp * 1000, '/');
			this.router.navigate(['/user']);
			// this.router.navigate(['/dashboard']);
		}
	}

	logout(): void {
		this.clear();
		this.router.navigate(['/login']);
	}

	decode(token?: string) {
		if (!token) {
			token = this.cookieService.get(this.tokenName);
		}
		if (!token) {
			return '';
		}
		return decode(token);
	}
}
